<?php

namespace SepCMS\Http\Controllers;

use Illuminate\Http\Request;
use SepCMS\Models\Permission;
use SepCMS\Models\Role;
use Illuminate\Support\Facades\Redirect;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $object = [
            'title' => '控制面板',
            'url'   => '/home',
        ];

        $data           = $request->all();
        $data['object'] = $object;
        $data['roles']  = Role::all();
        return view('role.index', $data);
    }

    public function create(Request $request)
    {
        $object = [
            'title' => '控制面板',
            'url'   => '/home',
        ];
        $data           = $request->all();
        $data['object'] = $object;
        return view('role.create', $data);
    }

    public function edit(Role $role)
    {
        $object = [
            'title' => '控制面板',
            'url'   => '/home',
        ];
        $data['role']        = $role;
        $data['object']      = $object;
        return view('role.edit', $data);
    }

    public function update(Role $role, Request $request)
    {
        $data = $request->all();
        if($role->update($data)){
            request()->session()->flash('message','角色更新成功');
            return Redirect::route('role.index');
        } else {
            request()->session()->flash('message','角色更新失败');
            return Redirect::route('role.edit', [$role->id]);
        }
    }

    public function store( Request $request)
    {
        $role = new Role();
        $data = $request->all();
        $role->name = $request->get('name');
        $role->guard_name = $request->get('guard_name');
        if ($role->save($data)) {
            request()->session()->flash('message','角色创建成功');
            return Redirect::route('role.index');
        } else {
            request()->session()->flash('message','角色创建失败');
            return Redirect::route('role.create');
        }
    }

    public function destroy(Role $role)
    {
        if ($role->delete()){
            request()->session()->flash('message','角色删除成功');
        } else {
            request()->session()->flash('message','角色删除失败');
        }

        return Redirect::route('role.index');
    }

    public function set(Role $role, Request $request)
    {
        $object = [
            'title' => '控制面板',
            'url'   => '/home',
        ];

        if($request->method() == 'POST' ) {
            $permissions = $request->get('permissions');
            if ($permissions) {
                $permissionsOBJ = [];
                foreach($permissions as $key => $permission) {
                    $permissionsOBJ[] = Permission::findById($permission);
                }

                if ($role->syncPermissions($permissionsOBJ)) {
                    request()->session()->flash('message','分配权限成功');
                } else {
                    request()->session()->flash('message','分配权限失败');
                }
            } else {
                request()->session()->flash('message', '请选择权限');
            }

        }

        $permissions       = new Permission();
        $abilities         = $permissions->menus();
        $data['abilities'] = $abilities;
        $data['role']      = $role;
        $data['object']    = $object;
        return view('role.set', $data );
    }


}
