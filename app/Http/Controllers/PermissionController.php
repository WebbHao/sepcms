<?php

namespace SepCMS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use SepCMS\Logic\Basic;
use SepCMS\Models\Permission;
use Route, DB;

class PermissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $object = [
            'title' => '控制面板',
            'url'   => '/home',
        ];

        $data           = $request->all();
        $data['object'] = $object;
        $data['permissions']  = Permission::paginate(config('paginate.page_size'));
        return view('permission.index', $data);
    }

    public function create(Request $request)
    {
        $data = $request->all();
        return view('permission.edit', $data);
    }

    public function edit(Permission $permission)
    {
        $object = [
            'title' => '控制面板',
            'url'   => '/home',
        ];
        $data['permission']  = $permission;
        $data['object']      = $object;
        return view('permission.edit', $data);
    }

    public function update(Permission $permission, Request $request)
    {
        $data = $request->all();
        if ($permission->update($data)){
            $request->session()->flash('message', '更新成功');
            return Redirect::route('permission.index',[$permission]);
        } else {
            $request->session()->flash('message', '更新失败，请重试');
            return Redirect::route('permission.edit',[$permission]);
        }
    }

    public function store(Permission $permission, Request $request)
    {

    }

    public function destroy(Permission $permission)
    {

    }

    public function show(Permission $permission)
    {

    }

    public function init()
    {
        //list all route name
        $routes = [];
        foreach (Route::getRoutes()->getRoutes() as $item){
            if (!starts_with($item->uri,'api') && $item->getName() && Basic::isAuthorization($item->getActionName())) {
                array_push($routes, $item->getName());
            }
        }

        $permissions = Permission::all();
        $exists      = [];
        foreach ($permissions as $item){
            if(!in_array($item->name, $routes)){
                $item->delete();
            } else {
                array_push($exists, $item->name);
            }
        }

        $news      = [];
        foreach($routes as $key => $val) {
            if (!in_array($val, $exists)) {
                array_push($news, $val);
            }
        }

        $abilities = [];
        if ($news) {
            foreach ($news as $index => $ability) {
                array_push($abilities,[
                    'name'       => $ability,
                    'guard_name' => 'web',
                    'status'     => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }

            DB::table('permissions')->insert($abilities);
        }

        request()->session()->flash('message','权限已经成功索引');
        return Redirect::route('permission.index');
    }
}
