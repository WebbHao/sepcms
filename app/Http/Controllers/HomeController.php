<?php

namespace SepCMS\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $object = [
            'title' => '控制面板',
            'url'   => '/home',
        ];

        $data['object'] = $object;
        return view('home', $data);
    }
}
