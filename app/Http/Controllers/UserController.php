<?php

namespace SepCMS\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use SepCMS\Models\Role;
use SepCMS\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $object = [
            'title' => '控制面板',
            'url' => '/home',
        ];

        $data['object'] = $object;
        $data['users'] = User::all();
        return view('user.index', $data);
    }

    public function create()
    {
        $data = [];
        return view('user.edit', $data);
    }

    public function edit(User $user, Request $request)
    {

    }

    public function update(User $user, Request $request)
    {

    }

    public function store(User $user, Request $request)
    {

    }

    public function destroy(User $user)
    {
        if ($user->delete()) {
            request()->session()->flash('message', '用户删除成功');
        } else {
            request()->session()->flash('message', '用户删除失败');
        }

        return Redirect::route('user.index');
    }

    public function set(User $user, Request $request)
    {
        $object = [
            'title' => '控制面板',
            'url' => '/home',
        ];

        if ($request->method() == 'POST') {
            $roles = $request->get('roles');
            if ($roles) {
                $rolesOBJ = [];
                foreach ($roles as $key => $role) {
                    $rolesOBJ[] = Role::findById($role);
                }

                if ($user->syncRoles($rolesOBJ)) {
                    request()->session()->flash('message', '分配权限成功');
                } else {
                    request()->session()->flash('message', '分配权限失败');
                }
            } else {
                request()->session()->flash('message', '请选择权限');
            }

        }

        $data['roles'] = Role::all();
        $data['user'] = $user;
        $data['object'] = $object;
        return view('user.set', $data);
    }

    public function resetPassword(User $user, Request $request)
    {
        $object = [
            'title' => '控制面板',
            'url' => '/home',
        ];

        if ($request->method() == 'POST') {

            $verification = Validator::make($request->all(), [
                'password'    => 'required|alpha_dash|between:6,12',
                're-password' => 'same:password',
            ])->validate();
            if (!$verification) {
                return redirect('user.reset-password',[$user])->withErrors($verification);
            }
            $user->password = Hash::make($request->get('password'));

            if ($user->save()) {
                request()->session()->flash('message', '重置密码成功');
            } else {
                request()->session()->flash('message', '重置密码失败');
            }

        }

        $data['roles'] = Role::all();
        $data['user'] = $user;
        $data['object'] = $object;
        return view('user.reset-password', $data);
    }
}
