<?php
/**
 * Created by PhpStorm.
 * User: justin
 * Date: 2017/9/15
 * Time: 11:47
 */

namespace SepCMS\Http\ViewComposers;
use SepCMS\Models\Permission;
use Illuminate\View\View;
use Illuminate\Support\Facades\Request;
class MenuComposer
{
    protected $abilityModel;

    public function __construct(Permission $abilityModel)
    {
        $this->abilityModel = $abilityModel;
    }

    public function compose(View $view)
    {
        $abilities = $this->abilityModel->menus(0);
        $view->with('routeName', Request::route()->getName());
        $view->with('abilities', $abilities);
    }

}
