<?php

namespace SepCMS\Models;

use Spatie\Permission\Models\Role as BasicRole;

class Role extends BasicRole
{

    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'guard_name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public  $_config = [

    ];

    /**
     * @todo    获取模型基础配置
     *
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public function getConfig()
    {
        return $this->_config;
    }

    public function abilities()
    {
        return $this->belongsToMany('App\Models\Ability')->withTimestamps();
    }
}
