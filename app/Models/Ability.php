<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth,Log;

class Ability extends Model
{
    protected $table = 'abilities';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'display_name', 'path_name', 'is_menu', 'status', 'grade',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public  $_config = [
        'channel'           => '权限管理',
        'title'             => '权限',
        'keywords'          => '账户维护',
        'description'       => '账户信息维护，设置角色、状态启用或禁用以及新建',
        'router'            => '/account/ability',           //路由
        'controller'        => 'Account\AbilityController',  //控制器
        'filter'            => true,                         //列表页是否开启条件搜索
        'pageSize'          => 20,                           //页面长度
        'orderBy'           => 'id',                         //排序字段
        'orderMethod'       => 'desc',                       //排序方式
        'templateIndex'     => 'account.ability.GetIndex',       //列表页模板
        'templateEdit'      => 'template.GetEdit',       //编辑、新建页模板
        'templateShow'      => 'template.GetShow',        //展示页模板
        'routeNamePrefix'   => 'ability',                 //路由名称前缀, 使用公共列别模板，该项必须配置
        'items'             => [
            'display_name' => [
                'title'     => '权限名称',
                'filter'    => true,
                'type'      => 'text',
                'validator' => 'required|max:50'
            ],
            'path_name'    => [
                'title'  => '路由名称',
                'filter' => true,
                'type'   => 'text',
            ],
            'class'    => [        //字段名称
                'title'  => '类名称',  //字段描述
                'filter' => false,        //是否做为列表页的筛选条件
                'type'   => 'text',  //编辑页控件类型
                'hidden' => false,       //列表页是否显示
            ],
            'is_menu'    => [          //字段名称
                'title'  => '菜单',   //字段描述
                'filter' => true,     //是否做为列表页的筛选条件
                'type'   => 'radio',  //编辑页控件类型
                'hidden' => false,    //列表页是否显示
                'selectItems' => [
                    '1'  => '是',
                    '0'  => '否',
                ],
            ],
            'sort'    => [
                'title'  => '排序',
                'filter' => true,
                'type'   => 'text',
            ],
            'status'    => [          //字段名称
                'title'  => '状态',   //字段描述
                'filter' => true,     //是否做为列表页的筛选条件
                'type'   => 'radio',  //编辑页控件类型
                'hidden' => false,    //列表页是否显示
                'selectItems' => [
                    '1'  => '启用',
                    '0'  => '禁用',
                ],
            ],
            'parent_id'    => [          //字段名称
                'title'  => '父级名称',   //字段描述
                'filter' => false,     //是否做为列表页的筛选条件
                'type'   => 'select',  //编辑页控件类型
                'hidden' => false,    //列表页是否显示
            ],
            'created_at'    => [          //字段名称
                'title'  => '创建时间',   //字段描述
                'filter' => false,     //是否做为列表页的筛选条件
                'type'   => 'text',  //编辑页控件类型
                'hidden' => false,    //列表页是否显示
            ],
            'updated_at'    => [          //字段名称
                'title'  => '更新时间',   //字段描述
                'filter' => false,     //是否做为列表页的筛选条件
                'type'   => 'text',  //编辑页控件类型
                'hidden' => false,    //列表页是否显示
            ],
        ],
    ];

    /**
     * @todo    获取模型基础配置
     *
     * @author  Justin.W<justin.bj@msn.com>
     * @return  mixed
     */
    public function getConfig()
    {
        $parents = self::where('is_menu','1')->where('status','1')->get();
        $parents = array_pluck($parents,'display_name','id');
        array_set($this->_config,'items.parent_id.selectItems',$parents);
        return $this->_config;
        return $this->_config;
    }

    /**
     * @todo 获取权限列表，按照级别递归结果
     *
     * @author Justin.W<justin.bj@msn.com>
     *
     * @param  int    $parent_id 父级id
     * @param  string $is_menu   标示 1 menu，0 all
     * @param  mixed  $existAbilities
     * @return mixed
     */
    public function menus($parent_id=0,$is_menu = '0',$existAbilities='')
    {
        $recursionList = [];
        try{
            if(Auth::user()->is_supper){
                $builder = self::where(function($query) use ($parent_id,$is_menu){
                    $query->where('status','1');
                    if($parent_id) $query->where('parent_id',$parent_id);
                    if($is_menu)   $query->where('is_menu',$is_menu);
                });
                $list = $builder->orderBy('sort')->get();
            }else{
                $list = Auth::user()->abilities();
            }
        }catch(QueryException $e){
            Log::info(__FILE__.' '.__LINE__.' ：'.$e->getMessage());
        }finally {
            if(!isset($list))
                $list = [];
        }
        if($list)
        {
            $list = $list->toArray();
            if($existAbilities){
                $existAbilities = $existAbilities->toArray();
                $abilitiesID    = array_pluck($existAbilities,'id');
                $tmp = [];
                array_map(function($val) use ($abilitiesID,&$tmp){
                    if(in_array($val['id'],$abilitiesID)){
                        $val['check_status'] = true;
                    }
                    $tmp[] = $val;
                    return true;
                },$list);
                $list = $tmp;
            }
            recursion($list,$recursionList,$parent_id);
        }
        return $recursionList;
    }

    public function roles()
    {
        return $this->belongsToMany("App\Models\Role")->withTimestamps();
    }

    public function parent()
    {
        return $this->hasOne('App\Models\Ability','id','parent_id');
    }

}
