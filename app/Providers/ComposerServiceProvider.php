<?php
/**
 * Created by PhpStorm.
 * User: justin
 * Date: 2017/9/15
 * Time: 11:54
 */

namespace SepCMS\Providers;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
class ComposerServiceProvider extends ServiceProvider
{

    public function boot()
    {
        View::composer('widget.menu', 'SepCMS\Http\ViewComposers\MenuComposer');
    }

    public function register(){}
}
