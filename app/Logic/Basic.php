<?php
/**
 * Created by PhpStorm.
 * User: Webb.M
 * Date: 2019/1/4
 * Time: 17:24
 */

namespace SepCMS\Logic;


class Basic
{
    private static $notAuthorizationList = [
        'Closure',
        'SepCMS\Http\Controllers\Auth\LoginController',
        'SepCMS\Http\Controllers\Auth\RegisterController',
        'SepCMS\Http\Controllers\Auth\ForgotPasswordController',
        'SepCMS\Http\Controllers\Auth\ResetPasswordController',
    ];

    /**
     * @todo   judgement action name
     * @param  $actionName
     * @return mixed
     */
    public static function isAuthorization($actionName)
    {
        $result = false;
        if ($actionName) {
            list($class, $func) = explode('@', $actionName);
        }
        if (!in_array($class, self::$notAuthorizationList)){
            $result = true;
        }
        return $result;
    }
}