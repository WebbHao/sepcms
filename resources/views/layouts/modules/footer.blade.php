<div class="pull-right hidden-xs">
    <b>SepCMS</b> 0.0.1
</div>
<strong>Copyright &copy; 2018-{{ date('Y') }} <a href="javascript:void(0);">Justin Wong</a>.</strong> All rights
reserved.
