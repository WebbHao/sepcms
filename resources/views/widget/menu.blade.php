<ul class="sidebar-menu" data-widget="tree">
    {{-- <li class="header">MAIN NAVIGATION</li> --}}
    <li class="header">{{__('labels.main navigation')}}</li>

    @if(is_array($abilities))
    @foreach($abilities as $i => $first)
    <li @if(isset($first['sub_list']) && !empty($first['sub_list'])) class="treeview" @elseif($routeName==$first['name']) class="active" @endif>
        <a href="{{starts_with($first['name'],'javascript')?'javascript:void(0);':route($first['name'])}}">
            <i class="{{$first['icon'] ? : 'fa fa-laptop'}}"></i> <span>{{$first['display_name'] ? : $first['name']}}</span>
            @if(isset($first['sub_list']) && !empty($first['sub_list']))
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            @endif
        </a>
        @if(isset($first['sub_list']) && !empty($first['sub_list']))
        <ul class="treeview-menu">
            @foreach($first['sub_list'] as $ii => $second)
            <li @if(isset($second['sub_list']) && !empty($second['sub_list'])) class="treeview" @elseif($routeName==$second['name']) class="active" @endif>
                <a href="{{starts_with($second['name'],'javascript')?'javascript:void(0);':route($second['name'])}}">
                    <i class="{{$second['icon'] ? : 'fa fa-laptop'}}"></i> {{$second['display_name'] ? : $second['name']}}
                    @if(isset($second['sub_list']) && !empty($second['sub_list']))
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    @endif
                </a>
                @if(isset($second['sub_list']) && !empty($second['sub_list']))
                <ul class="treeview-menu">

                    @foreach($scond['sub_list'] as $iii => $third)
                    <li @if(isset($third['sub_list']) && !empty($third['sub_list'])) class="treeview" @elseif($routeName==$third['name']) class="active" @endif>
                        <a href="{{starts_with($third['name'],'javascript')?'javascript:void(0);':route($third['name'])}}">
                            <i class="{{$third['icon'] ? : 'fa fa-laptop'}}"></i> {{$third['display_name'] ? : $third['name']}}
                            @if(isset($third['sub_list']) && !empty($third['sub_list']))
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                            @endif
                        </a>

                        @if(isset($third['sub_list']) && !empty($third['sub_list']))
                        <ul class="treeview-menu">
                            @foreach($third['sub_list'] as $iv => $four)
                            <li @if($routeName==$first['name']) class="active"@endif>
                                <a href="{{starts_with($four['name'],'javascript')?'javascript:void(0);':route($four['name'])}}">
                                    <i class="{{$four['icon'] ? : 'fa fa-laptop'}}"></i> {{$four['display_name'] ? : $four['name']}}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                        @endif
                    </li>
                    @endforeach
                </ul>
                @endif
            </li>
            @endforeach
        </ul>
        @endif
    </li>
    @endforeach
    @endif

</ul>
