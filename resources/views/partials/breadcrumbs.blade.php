@section("top-menu")
<h1>
    Dashboard
    <small>Control panel</small>
</h1>
    <ol class="breadcrumb">
        @if (count($breadcrumbs))
            @foreach($breadcrumbs as $breadcrumb)
                @if ($breadcrumb->url && $loop->first)
                    <li>
                        <a href="{{ $breadcrumb->url }}"><i class="fa fa-dashboard"></i> {{ $breadcrumb->title }}</a>
                    </li>
                @elseif ($breadcrumb->url && $loop->last)
                    <li class="active">
                        <a href={{ $breadcrumb->url }}> {{ $breadcrumb->title }}</a>
                    </li>
                @else
                    <li>{{ $breadcrumb->title }}</li>
                @endif
            @endforeach
        @endif

    </ol>
 @endsection

