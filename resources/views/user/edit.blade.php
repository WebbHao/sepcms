@extends('layouts.admin')

@section('top-menu')
    {{ Breadcrumbs::render('home', $object) }}
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="box col-md-8">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>

@endsection

