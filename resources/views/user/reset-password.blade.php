@extends('layouts.admin')

@section('top-menu')
    {{ Breadcrumbs::render('home', $object) }}
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">重置密码</h3>
                </div>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> {{ Session::get('message') }}</h4>

                    </div>
                @endif

                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{route('user.reset-password', [$user->id])}}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input hidden="id" name="id" value="{{$user->id}}" />
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">密码</label>

                            <div class="col-sm-5">
                                <input type="password" class="form-control" id="password" name="password"  value="">
                                {{ $errors->first('password') }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">确认密码</label>
                            <div class="col-sm-5">
                                <input type="password" class="form-control" id="re-password" name="re-password" value="">
                                {{ $errors->first('re-password') }}
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-info">保存</button>
                            <a href="{{route("user.index")}}" class="btn btn-default">返回</a>
                        </div>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
@endsection

