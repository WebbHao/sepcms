@extends('layouts.admin')

@section("css")
  <!-- DataTables -->
  <link rel="stylesheet" href="/resource/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('top-menu')
    {{ Breadcrumbs::render('system', $object) }}
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">管理员列表</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>用户名</th>
                    <th>邮箱</th>
                    <th>邮箱状态</th>
                    <th>状态</th>
                    <th>创建时间</th>
                    <th>更新时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                @if($users->count())
                <tbody>
                @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->verified_at ? '已验证' : '未验证' }}</td>
                    <td>{{ $user->status == 'enable' ? '启用' : '禁用' }}</td>
                    <td>{{ $user->created_at }}</td>
                    <td>{{ $user->updated_at }}</td>
                    <td>
                        <a href="{{route('user.set', [$user->id])}}">分配角色</a>|
                        <a href="{{route('user.reset-password', [$user->id])}}">重置密码</a>|
                        <form action="{{ route('user.destroy', [$user->id]) }}" method="POST" style="display: inline">
                            {{ csrf_field() }}{{ method_field('DELETE') }}
                            <a href="javascript:void(0);" class="btn-delete" >删除</a>
                        </form>
                    </td>
                </tr>
                @endforeach
                <tr>
                </tbody>
                @endif
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

@endsection


@section("js")
<!-- DataTables -->
<script src="/resource/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/resource/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {

    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
@endsection

