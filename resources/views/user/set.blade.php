@extends('layouts.admin')

@section('top-menu')
    {{ Breadcrumbs::render('home', $object) }}
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $user->name }}维护用户角色信息</h3>
                </div>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> {{ Session::get('message') }}</h4>

                    </div>
                @endif

                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                <form class="form-horizontal" action="{{route('user.set', [$user->id])}}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div id="menu-tree" class="box-body checkbox">
                        <ul style="list-style-type:none;">
                            @foreach($roles as $key => $role)
                            <li style="list-style-type:none; ">
                                <a href="#">
                                    <label>
                                        <input type="checkbox" data-role="checkbox" name="roles[]" @if($user->hasRole($role->name)) checked @endif value="{{$role->id}}">
                                        <span>{{$role->name}}</span>
                                    </label>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-info">保存</button>
                            <a href="{{route("user.index")}}" class="btn btn-default">返回</a>
                        </div>
                    </div>
                    <!-- /.box-footer -->
                </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script text="">
    function menuTreeInit(id)
    {
        $("#menu-tree ui")
    }

    $("#menu-tree")
</script>
@endsection