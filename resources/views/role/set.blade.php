@extends('layouts.admin')

@section('top-menu')
    {{ Breadcrumbs::render('home', $object) }}
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $role->name }}维护权限信息</h3>
                </div>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> {{ Session::get('message') }}</h4>

                    </div>
                @endif

                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                <form class="form-horizontal" action="{{route('role.set', [$role->id])}}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div id="menu-tree" class="box-body checkbox">
                        <ul style="list-style-type:none;">
                            @foreach($abilities as $key => $first)
                            <li style="list-style-type:none; ">
                                <a href="#">
                                    <label>
                                        <input type="checkbox" data-role="checkbox" name="permissions[]" @if($role->hasPermissionTo($first['name'])) checked @endif value="{{$first['id']}}">
                                        <span>{{$first['display_name'] ? : $first['name']}}</span>
                                    </label>
                                    @if(isset($first['sub_list']) && count($first['sub_list']))
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                    @endif
                                </a>
                                @if(isset($first['sub_list']) && count($first['sub_list']))
                                <ul style="list-style-type:none;display: block;">
                                    @foreach($first['sub_list'] as $i => $second)
                                    <li style="list-style-type:none;">
                                        <a href="#">
                                            <label>
                                                <input type="checkbox" name="permissions[]" @if($role->hasPermissionTo($second['name'])) checked @endif  value="{{$second['id']}}"> {{$second['display_name'] ? : $second['name']}}
                                            </label>
                                            @if(isset($second['sub_list']) && count($second['sub_list']))
                                            <span class="pull-right-container">
                                                <i class="fa fa-angle-left pull-right"></i>
                                            </span>
                                            @endif
                                        </a>
                                        @if(isset($second['sub_list']) && count($second['sub_list']))
                                        <ul style="list-style-type:none;">
                                            @foreach($second['sub_list'] as $ii => $third)
                                            <li style="list-style-type:none;">
                                                <a href="#">
                                                    <label>
                                                        <input type="checkbox" name="permissions[]" @if($role->hasPermissionTo($third['name'])) checked @endif value="{{$third['id']}}"> {{$third['display_name'] ? : $third['name']}}
                                                    </label>
                                                    @if(isset($third['sub_list']) && count($third['sub_list']))
                                                    <span class="pull-right-container">
                                                        <i class="fa fa-angle-left pull-right"></i>
                                                    </span>
                                                    @endif
                                                </a>
                                                @if(isset($third['sub_list']) && count($third['sub_list']))
                                                <ol style="list-style-type:none;">
                                                    @foreach($third['sub_list'] as $iii => $four)
                                                    <li>
                                                        <a href="{{action($four['name'])}}">
                                                            <label>
                                                                <input type="checkbox" name="permissions[]" @if($role->hasPermissionTo($four['name'])) checked @endif value="{{$four['id']}}"> {{$four['display_name'] ? : $four['name']}}
                                                            </label>
                                                        </a>
                                                    </li>
                                                    @endforeach
                                                </ol>
                                                @endif
                                            </li>
                                            @endforeach
                                        </ul>
                                        @endif
                                    </li>
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-info">保存</button>
                            <a href="{{route("role.index")}}" class="btn btn-default">返回</a>
                        </div>
                    </div>
                    <!-- /.box-footer -->
                </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script text="">
    function menuTreeInit(id)
    {
        $("#menu-tree ui")
    }

    $("#menu-tree")
</script>
@endsection