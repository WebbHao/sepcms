@extends('layouts.admin')

@section("css")
  <!-- DataTables -->
  <link rel="stylesheet" href="/resource/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('top-menu')
    {{ Breadcrumbs::render('system', $object) }}
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">角色列表</h3>
                <div class="pull-right">
                    <a class="btn btn-sm btn-success" href="{{ route('role.create') }}">创建角色</a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> {{ Session::get('message') }}</h4>
                    </div>
                @endif
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>序号</th>
                            <th>角色名称</th>
                            <th>GUARD</th>
                            <th>创建时间</th>
                            <th>更新时间</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    @if($roles->count())
                    <tbody>
                    @foreach($roles as $role)
                        <tr>
                            <td>{{ $role->id }}</td>
                            <td>{{ $role->name }}</td>
                            <td>{{ $role->guard_name }}</td>
                            <td>{{ $role->created_at }}</td>
                            <td>{{ $role->updated_at }}</td>
                            <td>
                                <a href="{{ route('role.set', [$role]) }}">分配权限</a>
                                <a href="{{route('role.edit', [$role])}}">编辑</a>
                                <form action="{{ route('role.destroy', [$role->id]) }}" method="POST" style="display: inline">
                                    {{ csrf_field() }}{{ method_field('DELETE') }}
                                    <a href="javascript:void(0);" class="btn-delete" >删除</a>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

@endsection


@section("js")
<!-- DataTables -->
<script src="/resource/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/resource/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

@endsection

