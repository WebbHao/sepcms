@extends('layouts.admin')

@section('top-menu')
    {{ Breadcrumbs::render('home', $object) }}
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">编辑角色信息</h3>
                </div>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> {{ Session::get('message') }}</h4>

                    </div>
                @endif

                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{route('role.update', [$role->id])}}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input hidden="hidden" name="_method" value="put" />
                    <input hidden="id" name="id" value="{{$role->id}}" />
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">角色名称</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Route Name" value="{{ $role->name }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Guard Name</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="guard_name" name="guard_name" placeholder="Guard Name" value="{{ $role->guard_name }}">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-info">保存</button>
                            <a href="{{route("role.index")}}" class="btn btn-default">返回</a>
                        </div>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
@endsection

