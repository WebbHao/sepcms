@extends('layouts.admin')

@section("css")
  <!-- DataTables -->
  <link rel="stylesheet" href="/resource/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('top-menu')
    {{ Breadcrumbs::render('system', $object) }}
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">权限列表</h3>
                    <div class="pull-right box-tools">
                        <a href="{{route('permission.init')}}">
                            <button type="button" class="btn btn-block btn-info">更新权限</button>
                        </a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> {{ Session::get('message') }}</h4>

                    </div>
                    @endif

                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>序号</th>
                                <th>权限</th>
                                <th>ICON</th>
                                <th>GUARD</th>
                                <th>显示名称</th>
                                <th>父级名称</th>
                                <th>菜单</th>
                                <th>状态</th>
                                <th>创建时间</th>
                                <th>更新时间</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        @if($permissions->count())
                        <tbody>
                            @foreach($permissions as $permission)
                            <tr>
                                <td>{{ $permission->id }}</td>
                                <td>{{ $permission->name }}</td>
                                <td>@if(!empty($permission->icon))<i class="{{ $permission->icon }}"></i>@else - @endif</td>
                                <td>{{ $permission->guard_name }}</td>
                                <td>{{ $permission->display_name }}</td>
                                <td>{{ $permission->parent_id }}</td>
                                <td>{{ $permission->is_menu }}</td>
                                <td>{{ $permission->status }}</td>
                                <td>{{ $permission->created_at }}</td>
                                <td>{{ $permission->updated_at }}</td>
                                <td>
                                    <a href="{{route('permission.edit',[$permission->id])}}">编辑</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        @endif
                    </table>
                        {{$permissions->links()}}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
      <!-- /.row -->

@endsection


@section("js")
<!-- DataTables -->
<script src="/resource/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/resource/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

@endsection

