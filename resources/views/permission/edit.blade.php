@extends('layouts.admin')

@section('top-menu')
    {{ Breadcrumbs::render('home', $object) }}
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">编辑权限信息</h3>
                </div>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> {{ Session::get('message') }}</h4>

                    </div>
                @endif

                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{route('permission.update', [$permission->id])}}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input hidden="hidden" name="_method" value="put" />
                    <input hidden="id" name="id" value="{{$permission->id}}" />
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">路由名称</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Route Name" value="{{ $permission->name }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Guard Name</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="guard_name" name="guard_name" placeholder="Guard Name" value="{{ $permission->guard_name }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">权限名称</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="display_name" name="display_name" placeholder="Display Name" value="{{ $permission->display_name }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">图标</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="icon" name="icon" placeholder="ICON" value="{{ $permission->icon }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">菜单</label>

                            <div class="col-sm-5">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="is_menu" id="is_menu" value="1" @if($permission->is_menu == '1') checked="checked" @endif> 是
                                    </label>

                                    <label class="col-sm-offset-1">
                                        <input type="radio" name="is_menu" id="is_menu" value="0" @if(empty($permission->is_menu)) checked="checked" @endif> 否
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">上级菜单编号</label>

                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="parent_id" name="parent_id" placeholder="Parent Menu ID" value="{{ $permission->parent_id }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">状态</label>

                            <div class="col-sm-5">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="status" id="status" value="1" @if($permission->status == '1') checked="checked" @endif> 启用
                                    </label>

                                    <label class="col-sm-offset-1">
                                        <input type="radio" name="status" id="status" value="0" @if(empty($permission->status)) checked="checked" @endif> 禁用
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-info">保存</button>
                            <button type="button" class="btn btn-default">返回</button>
                        </div>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
@endsection

