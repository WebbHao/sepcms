<?php
/**
 * Created by PhpStorm.
 * User: justin
 * Date: 2017/9/15
 * Time: 14:30
 */

if (! function_exists('chunk')) {
    /**
     * @todo   从数组中截取部分元素
     *
     * @author Justin.W<justin.bj@msn.com>
     * @param  [type] &$list    被整理数组地址引用
     * @param  [type] $parentID 父类id
     * @param  string $conditionKey 检索字段
     * @return mixed            子结果
     */
    function chunk(&$list,$parentID,$conditionKey){
        $tmp = $list;
        $recursion = [];
        foreach($tmp as $key => $val){
            if(isset($val[$conditionKey]) && $val[$conditionKey]===$parentID){
                array_push($recursion,$val);
                unset($list[$key]);
            }
        }
        return $recursion;
    };
}

if(! function_exists('recursion')){
    /**
     * @todo  对指定的数组递归调用
     *
     * @author Justin.W<justin.w@baicheng.com>
     * @param  array $list          被递归的数组
     * @param  array $recursionList 父级id集合
     * @param  int   $parentID      父级id集合
     * @param  string $conditionKey 检索字段
     * @return mixed
     */
    function recursion(&$list,&$recursionList=[],$parentID=0,$conditionKey='parent_id')
    {
        if(is_array($list)&&count($list)){

            $subRecursionList = chunk($list,$parentID,$conditionKey);
            if(count($recursionList)){
                $recursionList['sub_list'] = $subRecursionList;
            }else{
                $recursionList = $subRecursionList;
            }
        }else{
            return $recursionList;
        }
        if(is_array($subRecursionList)&&count($subRecursionList)){
            foreach($subRecursionList as $key => $val)
            {
                //递归调用
                if(array_key_exists('sub_list',$recursionList)){
                    recursion($list,$recursionList['sub_list'][$key],$val['id']);
                }else{
                    recursion($list,$recursionList[$key],$val['id']);
                }
            }
        }
        return $recursionList;
    }
}


if( ! function_exists('menu_path')){
    function menu_path($menu){
        $path = [];
        if($menu && is_array($menu) && count($menu)) {
            $path[] = array_get($menu,'path_name');
            if(array_get($menu,'sub_list') && is_array($menu['sub_list']) && count($menu['sub_list'])){
                foreach($menu['sub_list'] as $key => $val){
                    $path[] = array_get($val,'path_name');
                    if(array_get($val,'sub_list') && is_array($val['sub_list']) && count($val['sub_list'])){
                        $subPath = array_pluck($val['sub_list'],'path_name');
                        $path    = array_merge($path,$subPath);
                    }
                }
            }

        }
        return array_unique($path);
    }
}
