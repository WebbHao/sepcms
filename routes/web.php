<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('user', 'UserController');
Route::resource('role', 'RoleController');
Route::resource('permission', 'PermissionController')->except(['show']);
Route::get('permission/init','PermissionController@init')->name('permission.init');
//Route::any('role/set/{role}', 'RoleController@set')->name('role.set');
Route::match(['get', 'post'],'role/set/{role}','RoleController@set')->name('role.set');
Route::match(['get', 'post'],'user/set/{user}','UserController@set')->name('user.set');
Route::match(['get', 'post'],'user/reset-password/{user}','UserController@resetPassword')->name('user.reset-password');
