<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions', function($table){
            $table->string("display_name")->after("guard_name")->default('')->comment('权限显示名称');
            $table->enum("is_menu",['1','0'])->after("guard_name")->default('0');
            $table->string("icon")->after("is_menu")->default('')->comment('图标');
            $table->enum("status", ['1','0'])->after("icon")->default('0')->comment('状态');
            $table->integer("sort")->after("status")->default('0')->comment('排序');
            $table->integer("parent_id")->after("status")->default('0')->comment('父级ID');
        });

        if (Schema::hasTable('users')) {
            Schema::table('user', function($table) {
                $table->enum("status",['enable','disable'])->after("remember_token")->default('enable')->comment('状态');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function($table){
            $table->dropColumn(['display_name','is_menu','icon','status','parent_id']);
        });

        if (Schema::hasTable('users')) {
            Schema::table('user', function($table) {
                $table->dropColumb(['status']);
            });
        }
    }
}
