<?php
/**
 * Created by PhpStorm.
 * User: Webb.M
 * Date: 2019/1/8
 * Time: 11:24
 */

return [
    'page_size'  => env('PAGE_SIZE', 20),
];